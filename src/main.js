//import './assets/scss/themes/default.scss';

import Vue from 'vue';
import App from './App.vue';
import wsuduLib from './lib.js';
import marked from 'marked';
import Prism from 'prismjs';

window.Vue = Vue;
window.wsuduLib = wsuduLib;
window.marked = marked;
window.Prism = Prism;


window.wsudu = new Vue({
    el: '#wsudu',
    data: {
        navigation: {},
        components: {},
        openCategory: wsuduLib.getUrlVars('c') ? wsuduLib.getUrlVars('c') : '',
        filterNavigation: '',
        title: wsuduCgf.title ? wsuduCgf.title : '',
        showAll: wsuduCgf.showAll === undefined ||  wsuduCgf.showAll ? true : false,
        openSidebar: true
    },
    render: h => h(App)
})
