var wsuduLib = {
    alwaysArray: function (value) {
        if (!Array.isArray(value)) {
            value = [value];
        }
        return value;
    },

    getJSON: function (url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'json';
        xhr.onload = function () {
            var status = xhr.status;
            if (status == 200) {
                callback(xhr.response, status);
            } else {
                callback(null, status);
            }
        };
        xhr.send();
    },

    postJSON: function (url, object, callback) {
        var xhr = new XMLHttpRequest();
        var url = url;
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                if (callback) {
                    callback(xhr.responseText);
                }
            }
        };
        var data = JSON.stringify(object);
        xhr.send(data);
    },

    isObject: function (item) {
        return (item && typeof item === 'object' && !Array.isArray(item));
    },

    mergeDeep: function (target, source) {
        let output = Object.assign({}, target);
        if (wsuduLib.isObject(target) && wsuduLib.isObject(source)) {
            Object.keys(source).forEach(key => {
                if (wsuduLib.isObject(source[key])) {
                    if (!(key in target))
                        Object.assign(output, {
                            [key]: source[key]
                        });
                    else
                        output[key] = wsuduLib.mergeDeep(target[key], source[key]);
                } else {
                    Object.assign(output, {
                        [key]: source[key]
                    });
                }
            });
        }
        return output;
    },

    getArrayNavigation: function (data) {
        var navigationDrafts = [];
        var navigationComplete = {};

        Object.keys(data).forEach(function (key) {
            var category = data[key]['category'];

            wsuduLib.alwaysArray(category).forEach(function (value) {
                navigationDrafts.push(value);
            });
        });

        navigationDrafts = navigationDrafts.filter(function (item, pos, self) {
            return self.indexOf(item) == pos;
        }).sort();

        var compositionObject = function (navigationDrafts) {
            var returnObject = {};
            var splitItem = navigationDrafts.split('/');

            if (splitItem.length > 1) {
                returnObject[splitItem[0]] = compositionObject(splitItem.slice(1, splitItem.length).join('/'));
            } else {
                returnObject[splitItem[0]] = {};
            }

            return returnObject;
        };

        navigationDrafts.forEach(function (item) {
            navigationComplete = wsuduLib.mergeDeep(navigationComplete, compositionObject(item));
        });

        return navigationComplete;
    },

    requestCache: {
        prefix: 'rc#',
        set: function (key, request, response) {
            if (!window.localStorage) {
                return false;
            }

            localStorage.setItem(this.prefix + key, JSON.stringify({
                request: request,
                response: response
            }));
        },
        get: function (key, request) {
            if (!window.localStorage) {
                return false;
            }

            var lastRequest = localStorage.getItem(this.prefix + key);

            if (lastRequest) {
                lastRequest = JSON.parse(lastRequest);

                if (lastRequest.request == request) {
                    return lastRequest.response;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    },
    getUrlVars: function (key) {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars[key];
    },
    setUrlVars: function (key, value) {
        key = encodeURI(key);
        value = encodeURI(value);

        var kvp = window.location.hash.substr(2).split('&');

        var i = kvp.length;
        var x;
        while (i--) {
            x = kvp[i].split('=');

            if (x[0] == key) {
                x[1] = value;
                kvp[i] = x.join('=');
                break;
            }
        }

        if (i < 0) {
            kvp[kvp.length] = [key, value].join('=');
        }

        window.location.hash = '?' + kvp.join('&');
    },
    getFileExtension: function (name) {
        return name.split('.').pop();
    },
    getWsuduDataJson: function (pathData, callback) {
        this.getJSON(pathData, function (response, status) {
            if (response == null) {
                console.error(pathData + ' is no valid'); 
            } else {
                callback(response);
            }    
        });
    },
    getWsuduDataJs: function (pathData, callback) {
        var script = document.createElement('script');
        script.onload = function() {
            callback(wsuduData);
        };
        script.src = pathData;
        document.getElementsByTagName('head')[0].appendChild(script);
    },
    getWsuduData: function (pathData, callback) {
        var extension = this.getFileExtension(pathData);

        if (extension == 'js') {
            this.getWsuduDataJs(pathData, callback);
        } else if (extension == 'json') {
            this.getWsuduDataJson(pathData, callback);
        } else {
            console.error(pathData + ' is no valid');   
        }
    }
};

module.exports = wsuduLib;
